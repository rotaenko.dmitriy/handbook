---
title: "Engineering Career Framework: Associate"
---

## Engineering Function Competencies: Associate
 
**Associates at GitLab are expected to exhibit the following competencies:**

- [Associate Leadership Competencies](#associate-leadership-competencies)
- [Associate Technical Competencies](#associate-technical-competencies)
- [Associate Values Alignment](#associate-values-alignment)

---

### Associate Leadership Competencies

{{% include "includes/engineering/associate-leadership-competency.md" %}}
  
### Associate Technical Competencies

{{% include "includes/engineering/associate-technical-competency.md" %}}

### Associate Values Alignment

{{% include "includes/engineering/values-competency.md" %}}
